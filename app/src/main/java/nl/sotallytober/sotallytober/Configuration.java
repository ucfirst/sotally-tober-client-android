package nl.sotallytober.sotallytober;

import nl.sotallytober.sotallytober.http.Connection;

/**
 * Application wide constants
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 20-05-15
 */
public final class Configuration {

    public static final boolean DEBUG = true;

    public static final boolean DEBUG_HTTP = true;

    public static final String APP_SLUG = "SotallyTober";

    public static final String API_BASE_URL = "sotallytober.nl/api";

    public static final String API_COOKIE_SESSION_KEY = "ucfirst_cms";

    public static final String API_COOKIE_REMEMBER_ME_PREFIX = "remember_";

    public static final String API_VERSION = "1";

}
