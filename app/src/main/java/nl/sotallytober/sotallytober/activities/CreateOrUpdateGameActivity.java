package nl.sotallytober.sotallytober.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.tasks.CreateGameAsyncTask;
import nl.sotallytober.sotallytober.tasks.UpdateGameAsyncTask;

public class CreateOrUpdateGameActivity extends ActionBarActivity implements
        CreateGameAsyncTask.OnCreateGameAsyncTaskListener,
        UpdateGameAsyncTask.OnUpdateGameAsyncTaskListener {

    public static final int REQUEST_CODE = 0;

    public static final int RESULT_CREATE_COMPLETED = 1;

    public static final int RESULT_UPDATE_COMPLETED = 2;

    public static final int RESULT_CANCELLED = 3;

    private UpdateGameAsyncTask mUpdateGameAsyncTask;

    private CreateGameAsyncTask mCreateGameAsyncTask;

    public GameModel mGame = new GameModel();

    private EditText mNameView;

    private EditText mDescriptionView;

    private Spinner mTypeView;

    private Spinner mPrivateView;

    private ProgressDialog mSaveDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_or_update_game);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNameView = (EditText) findViewById(R.id.create_or_update_game_name);
        mDescriptionView = (EditText) findViewById(R.id.create_or_update_game_description);
        mTypeView = (Spinner) findViewById(R.id.create_or_update_game_type);
        mPrivateView = (Spinner) findViewById(R.id.create_or_update_game_private);

        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this, R.array.activity_create_or_update_game_type_array_values, android.R.layout.simple_spinner_dropdown_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeView.setAdapter(typeAdapter);

        ArrayAdapter<CharSequence> privateAdapter = ArrayAdapter.createFromResource(this, R.array.activity_create_or_update_game_private_array_values, android.R.layout.simple_spinner_dropdown_item);
        privateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPrivateView.setAdapter(privateAdapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            Integer gameId = extras.getInt(GameMigration.COLUMN_ID);
            setGame((GameModel) GameProvider.get().selectFirst(gameId));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                quitWithQuestionDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        quitWithQuestionDialog();
    }

    private void quitWithQuestionDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        saveAndQuit();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        quit(RESULT_CANCELLED);
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrUpdateGameActivity.this);
        builder.setMessage(getString(R.string.question_save_changes))
                .setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener)
                .show();
    }

    private void setGame(GameModel mGame) {
        this.mGame = mGame;

        mNameView.setText(mGame.getName());
        mDescriptionView.setText(mGame.getDescription());
    }

    private GameModel getGameWithNewDetails() {
        mGame.setName(mNameView.getText().toString());
        mGame.setDescription(mDescriptionView.getText().toString());

        Spinner typeSpinner = (Spinner) findViewById(R.id.create_or_update_game_type);
        String[] typeKeys = getResources().getStringArray(R.array.activity_create_or_update_game_type_array_keys);
        String typeSelected = typeKeys[typeSpinner.getSelectedItemPosition()];
        mGame.setType(typeSelected);

        Spinner privateSpinner = (Spinner) findViewById(R.id.create_or_update_game_private);
        String[] privateKeys = getResources().getStringArray(R.array.activity_create_or_update_game_private_array_keys);
        String privateSelected = privateKeys[privateSpinner.getSelectedItemPosition()];
        mGame.setIsPrivate(privateSelected.equals("1"));

        return mGame;
    }

    private void saveAndQuit() {
        if (mUpdateGameAsyncTask == null && mGame.existsLocally()) {
            mSaveDialog = new ProgressDialog(CreateOrUpdateGameActivity.this);
            mSaveDialog.setTitle(getString(R.string.status_saving));
            mSaveDialog.setCancelable(false);
            mSaveDialog.show();

            mUpdateGameAsyncTask = new UpdateGameAsyncTask(this, getGameWithNewDetails());
            mUpdateGameAsyncTask.execute();
        } else if(mCreateGameAsyncTask == null) {
            mSaveDialog = new ProgressDialog(CreateOrUpdateGameActivity.this);
            mSaveDialog.setTitle(getString(R.string.status_saving));
            mSaveDialog.setCancelable(false);
            mSaveDialog.show();

            mCreateGameAsyncTask = new CreateGameAsyncTask(this, getGameWithNewDetails());
            mCreateGameAsyncTask.execute();
        }
    }

    private void quit(Integer result) {
        if(result != null) {
            Intent intent = new Intent();
            intent.putExtra(GameMigration.COLUMN_ID, mGame.getId());
            setResult(result, intent);
        }

        finish();
    }

    @Override
    public void onUpdateGameAsyncTaskCompleted() {
        mSaveDialog.dismiss();
        quit(RESULT_UPDATE_COMPLETED);
    }

    @Override
    public void onUpdateGameAsyncTaskFailed(GameModel mOldRow) {
        mSaveDialog.dismiss();
        Toast.makeText(this, "onUpdateGameAsyncTaskFailed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateGameAsyncTaskCompleted(GameModel newGame) {
        mGame = newGame;
        mSaveDialog.dismiss();
        quit(RESULT_CREATE_COMPLETED);
    }

    @Override
    public void onCreateGameAsyncTaskFailed(Response mResponse) {
        mSaveDialog.dismiss();

        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(this, mResponse.getMessage(getApplicationContext()), Toast.LENGTH_LONG).show();
        } else if(mResponse.getStatus().equals(Response.STATUS_ERROR_VALIDATION)) {
            for (Map.Entry<String, ArrayList<String>> entry : mResponse.getValidation().entrySet()) {
                switch(entry.getKey()) {
                    case "name":
                        mNameView.setError(entry.getValue().get(0));
                        mNameView.requestFocus();
                        break;
                    case "description":
                        mDescriptionView.setError(entry.getValue().get(0));
                        mDescriptionView.requestFocus();
                    case "type":
                        Toast.makeText(this, entry.getValue().get(0), Toast.LENGTH_SHORT).show();
                    case "private":
                        Toast.makeText(this, entry.getValue().get(0), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
