package nl.sotallytober.sotallytober.activities;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.validators.UserValidator;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;
import nl.sotallytober.sotallytober.tasks.LoginUserAsyncTask;

/**
 * Login activity
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class LoginActivity extends Activity implements
        LoginUserAsyncTask.OnUserLoginAsyncTaskListener {

    private LoginUserAsyncTask mAuthenticationTask = null;

    private EditText mEmailView;

    private EditText mPasswordView;

    private ScrollView mProgressView;

    private ScrollView mLoginFormView;

    private Button mLoginButton;

    private TextView mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (EditText) findViewById(R.id.login_email);
        mPasswordView = (EditText) findViewById(R.id.login_password);
        mLoginFormView = (ScrollView) findViewById(R.id.login_form);
        mProgressView = (ScrollView) findViewById(R.id.login_progress);
        mLoginButton = (Button) findViewById(R.id.login_button);
        mRegisterButton = (TextView) findViewById(R.id.login_register_button);

        mLoginButton.setOnClickListener(onLoginButtonClick);
        mRegisterButton.setOnClickListener(onRegisterTextClick);

        mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
    }

    public OnClickListener onLoginButtonClick = new OnClickListener(){

        @Override
        public void onClick(View v) {
            attemptLogin();
        }

    };

    public OnClickListener onRegisterTextClick = new OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(registerIntent);
            finish();
        }

    };

    public void attemptLogin() {
        if (mAuthenticationTask != null) {
            return;
        }

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        mEmailView.setError(null);
        if(!UserValidator.isValidEmail(email)) {
            mEmailView.setError(getString(R.string.activity_login_error_invalid_email));
            mEmailView.requestFocus();
            return;
        }

        mPasswordView.setError(null);
        if(!UserValidator.isValidPassword(password)) {
            mPasswordView.setError(getString(R.string.activity_login_error_invalid_password));
            mPasswordView.requestFocus();
            return;
        }

        showProgress(true);
        mAuthenticationTask = new LoginUserAsyncTask(this, email, password);
        mAuthenticationTask.execute();
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onUserLoginAsyncTaskSuccess(UserModel user) {
        Auth.setUser(user);

        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(mainIntent);

        finish();
    }

    @Override
    public void onUserLoginAsyncTaskFailed(Response mResponse) {
        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(this, mResponse.getMessage(getApplicationContext()), Toast.LENGTH_LONG).show();
        } else if(mResponse.getStatus().equals(Response.STATUS_ERROR_VALIDATION)) {
            for (Map.Entry<String, ArrayList<String>> entry : mResponse.getValidation().entrySet()) {
                switch(entry.getKey()) {
                    case "email":
                        mEmailView.setError(entry.getValue().get(0));
                        mEmailView.requestFocus();
                        break;
                    case "password":
                        mPasswordView.setError(entry.getValue().get(0));
                        mPasswordView.requestFocus();
                }
            }
        }

        stopAuthenticationTask();
    }

    public void stopAuthenticationTask() {
        mAuthenticationTask = null;
        showProgress(false);
    }

}



