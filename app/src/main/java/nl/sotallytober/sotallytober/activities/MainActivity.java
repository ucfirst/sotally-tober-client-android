package nl.sotallytober.sotallytober.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.support.v4.widget.DrawerLayout;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.DatabaseHandler;
import nl.sotallytober.sotallytober.fragments.AccountFragment;
import nl.sotallytober.sotallytober.fragments.ChallengesFragment;
import nl.sotallytober.sotallytober.fragments.ExploreFragment;
import nl.sotallytober.sotallytober.fragments.GamesFragment;
import nl.sotallytober.sotallytober.fragments.NavigationFragment;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;

/**
 * Main activity
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class MainActivity extends ActionBarActivity implements
        NavigationFragment.NavigationDrawerCallbacks {

    private NavigationFragment mNavigationDrawerFragment;

    private int mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseHandler.initialize(getApplicationContext());
        Auth.sync();

        if(!Auth.isLoggedIn()) {
            logoutAndShowLogin();
        } else {
            setContentView(R.layout.activity_main);
            mNavigationDrawerFragment = (NavigationFragment)
                    getSupportFragmentManager().findFragmentById(R.id.main_navigation_drawer);
            mNavigationDrawerFragment.setUp(R.id.main_navigation_drawer,
                    (DrawerLayout) findViewById(R.id.main_drawer_layout));
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch(position) {
            case 0:
                fragmentManager.beginTransaction().replace(R.id.main_container,
                        new GamesFragment()).commit();
                break;
            case 1:
                fragmentManager.beginTransaction().replace(R.id.main_container,
                        new ExploreFragment()).commit();
                break;
            case 2:
                fragmentManager.beginTransaction().replace(R.id.main_container,
                        new ChallengesFragment()).commit();
                break;
            case 3:
                fragmentManager.beginTransaction().replace(R.id.main_container,
                        new AccountFragment()).commit();
                break;
        }

    }

    public void logoutAndShowLogin() {
        Auth.logout();

        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(loginIntent);

        finish();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(mTitle));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    public void onSectionAttached(int mTitle) {
        this.mTitle = mTitle;
    }

}
