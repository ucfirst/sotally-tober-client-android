package nl.sotallytober.sotallytober.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.validators.UserValidator;
import nl.sotallytober.sotallytober.fragments.DatePickerFragment;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;
import nl.sotallytober.sotallytober.tasks.RegisterUserAsyncTask;

/**
 * Register activity
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class RegisterActivity extends Activity implements
        RegisterUserAsyncTask.OnUserRegisterAsyncTaskListener, DatePickerDialog.OnDateSetListener {

    private RegisterUserAsyncTask mAuthenticationTask = null;

    private EditText mEmailView;

    private EditText mPasswordView;

    private EditText mPasswordConfirmationView;

    private EditText mFirstName;

    private EditText mInsertionView;

    private EditText mLastNameView;

    private EditText mDateOfBirthView;

    private ScrollView mProgressView;

    private ScrollView mRegisterFormView;

    private Button mRegisterButton;

    private TextView mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEmailView = (EditText) findViewById(R.id.register_email);
        mPasswordView = (EditText) findViewById(R.id.register_password);
        mPasswordConfirmationView = (EditText) findViewById(R.id.register_password_confirmation);
        mFirstName = (EditText) findViewById(R.id.register_first_name);
        mInsertionView = (EditText) findViewById(R.id.register_insertion);
        mLastNameView = (EditText) findViewById(R.id.register_last_name);
        mDateOfBirthView = (EditText) findViewById(R.id.register_date_of_birth);

        mRegisterFormView = (ScrollView) findViewById(R.id.register_form);
        mProgressView = (ScrollView) findViewById(R.id.register_progress);
        mRegisterButton = (Button) findViewById(R.id.register_button);
        mLoginButton = (TextView) findViewById(R.id.register_login_button);

        mRegisterButton.setOnClickListener(onRegisterButtonClick);
        mLoginButton.setOnClickListener(onLoginTextClick);

        mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
        mPasswordConfirmationView.setTransformationMethod(new PasswordTransformationMethod());
    }

    public OnClickListener onRegisterButtonClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            attemptRegister();
        }

    };

    public OnClickListener onLoginTextClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            showLogin();
        }

    };

    @Override
    public void onBackPressed() {
        showLogin();
    }

    private void showLogin() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment dateFragment = new DatePickerFragment();
        Bundle arguments = new Bundle();
        arguments.putString("date", mDateOfBirthView.getText().toString());
        dateFragment.setArguments(arguments);

        dateFragment.show(getFragmentManager(), "DatePicker");
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        SimpleDateFormat mysql = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new GregorianCalendar(year, month, day).getTime();

        mDateOfBirthView.setText(mysql.format(date));
    }

    public void attemptRegister() {
        if (mAuthenticationTask != null) {
            return;
        }

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String passwordConfirmation = mPasswordConfirmationView.getText().toString();
        String firstName = mFirstName.getText().toString();
        String insertion = mInsertionView.getText().toString();
        String lastName = mLastNameView.getText().toString();
        String dateOfBirth = mDateOfBirthView.getText().toString();

        mEmailView.setError(null);
        mPasswordView.setError(null);
        mPasswordConfirmationView.setError(null);
        mFirstName.setError(null);
        mInsertionView.setError(null);
        mLastNameView.setError(null);

        if(!UserValidator.isValidEmail(email)) {
            mEmailView.setError(getString(R.string.activity_register_error_invalid_email));
            mEmailView.requestFocus();
            return;
        }

        if(!UserValidator.isValidPassword(password)) {
            mPasswordView.setError(getString(R.string.activity_register_error_invalid_password));
            mPasswordView.requestFocus();
            return;
        }

        if(!password.equals(passwordConfirmation)) {
            mPasswordConfirmationView.setError(getString(R.string.activity_register_error_invalid_password_confirmation));
            mPasswordConfirmationView.requestFocus();
            return;
        }

        showProgress(true);
        mAuthenticationTask = new RegisterUserAsyncTask(this, email, password, passwordConfirmation, firstName, insertion, lastName, dateOfBirth);
        mAuthenticationTask.execute();
    }

    @Override
    public void onUserRegisterAsyncTaskSuccess(UserModel user) {
        Auth.setUser(user);

        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(mainIntent);

        finish();
    }

    @Override
    public void onUserRegisterAsyncTaskFailed(Response mResponse) {
        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(this, mResponse.getMessage(getApplicationContext()), Toast.LENGTH_LONG).show();
        } else if(mResponse.getStatus().equals(Response.STATUS_ERROR_VALIDATION)) {
            for (Map.Entry<String, ArrayList<String>> entry : mResponse.getValidation().entrySet()) {
                switch(entry.getKey()) {
                    case "email":
                        mEmailView.setError(entry.getValue().get(0));
                        mEmailView.requestFocus();
                        break;
                    case "password":
                        mPasswordView.setError(entry.getValue().get(0));
                        mPasswordView.requestFocus();
                        break;
                    case "password_confirmation":
                        mPasswordConfirmationView.setError(entry.getValue().get(0));
                        mPasswordConfirmationView.requestFocus();
                        break;
                    case "first_name":
                        mFirstName.setError(entry.getValue().get(0));
                        mFirstName.requestFocus();
                        break;
                    case "insertion":
                        mInsertionView.setError(entry.getValue().get(0));
                        mInsertionView.requestFocus();
                        break;
                    case "last_name":
                        mLastNameView.setError(entry.getValue().get(0));
                        mLastNameView.requestFocus();
                        break;
                }
            }
        }

        stopAuthenticationTask();
    }

    public void stopAuthenticationTask() {
        mAuthenticationTask = null;
        showProgress(false);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

}



