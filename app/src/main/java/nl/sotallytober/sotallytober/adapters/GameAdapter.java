package nl.sotallytober.sotallytober.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.models.BaseModel;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;

/**
 * Game adapter
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 14-05-15
 */
public class GameAdapter extends ArrayAdapter<BaseModel> {

    private ArrayList<BaseModel> items;

    public GameAdapter(Context context, ArrayList<BaseModel> items) {
        super(context, R.layout.fragment_games_item, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.fragment_games_item, null);
        }

        GameModel game = (GameModel) items.get(position);

        // Game name
        TextView name = (TextView) view.findViewById(R.id.game_item_name);
        if (name != null){
            name.setText(game.getName());
        }

        // Game author
        TextView owner = (TextView) view.findViewById(R.id.game_item_user_full_name);
        UserModel ownerModel = game.getOwner();
        if (owner != null && ownerModel != null){
            owner.setText(" " + ownerModel.getFullName() + " ");
        }

        // Game creation date
        TextView date = (TextView) view.findViewById(R.id.game_item_created_at);
        if (date != null){
            date.setText(" " + game.getCreatedAt());
        }

        // Game description
        TextView description = (TextView) view.findViewById(R.id.game_item_description);
        if (description != null){
            description.setText(game.getDescription());
        }

        // Game connection type
        ImageView type = (ImageView) view.findViewById(R.id.game_item_game_user_type);
        if (type != null){
            if(game.getUserId().equals(Auth.getUser().getId())) {
                type.setImageResource(R.drawable.ic_action_person);
            } else {
                type.setImageResource(R.drawable.ic_action_important);
            }
        }

        return view;
    }

}