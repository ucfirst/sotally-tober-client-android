package nl.sotallytober.sotallytober.comparators;

import java.util.Comparator;

import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.models.BaseModel;
import nl.sotallytober.sotallytober.database.models.GameModel;

/**
 * Sort GameModel by the given column & type
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 17-05-15
 */
public class GamesComparator implements Comparator<BaseModel> {

    private final String ORDER_TYPE_ASC = "ASC";

    private final String ORDER_TYPE_DESC = "DESC";

    private final String ORDER_COLUMN;

    private final String ORDER_TYPE;

    public GamesComparator() {
        this.ORDER_COLUMN = GameMigration.COLUMN_NAME;
        this.ORDER_TYPE = ORDER_TYPE_ASC;
    }

    public GamesComparator(String column, String type) {
        this.ORDER_COLUMN = column;
        this.ORDER_TYPE = type;
    }

    @Override
    public int compare(BaseModel item1, BaseModel item2) {
        GameModel game1 = (GameModel) item1;
        GameModel game2 = (GameModel) item2;

        switch(ORDER_COLUMN) {
            case GameMigration.COLUMN_NAME:
                return compareByName(game1, game2);
            default:
                throw new IllegalArgumentException("Column order on " + ORDER_COLUMN
                        + " not yet implemented in GamesComparator");
        }
    }

    private int compareByName(GameModel game1, GameModel game2) {
        switch(ORDER_TYPE) {
            case ORDER_TYPE_ASC:
                return game1.getName().compareTo(game2.getName());
            case ORDER_TYPE_DESC:
                return game2.getName().compareTo(game1.getName());
            default:
                throw new IllegalArgumentException("Type order on " + ORDER_COLUMN + "::"
                        + ORDER_TYPE + " not yet implemented in GamesComparator");
        }
    }

}