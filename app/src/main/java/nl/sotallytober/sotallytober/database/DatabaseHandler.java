package nl.sotallytober.sotallytober.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.migrations.UserMigration;
import nl.sotallytober.sotallytober.database.providers.GameProvider;
import nl.sotallytober.sotallytober.database.providers.UserProvider;

/**
 * Database handler
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static DatabaseHandler sInstance;

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "sotally_tober.dat";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void initialize(Context context) {
        sInstance = new DatabaseHandler(context);
    }

    public static synchronized DatabaseHandler get() {
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        UserMigration.createTable(db);
        GameMigration.createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop old tables
        UserMigration.dropTable(db);
        GameMigration.dropTable(db);

        // Create new tables
        UserMigration.createTable(db);
        GameMigration.createTable(db);
    }

    public void emptyDatabase() {
        UserProvider.get().deleteAll();
        GameProvider.get().deleteAll();
    }

}
