package nl.sotallytober.sotallytober.database.migrations;

import android.database.sqlite.SQLiteDatabase;

/**
 * Game migration
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class GameMigration {

    public static final String TABLE_NAME = "games";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_IS_PRIVATE = "is_private";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_UPDATED_AT = "updated_at";

    public static final String TYPE_TASK = "task";

    public static final String[] COLUMNS = {
            COLUMN_ID,
            COLUMN_USER_ID,
            COLUMN_NAME,
            COLUMN_DESCRIPTION,
            COLUMN_TYPE,
            COLUMN_IS_PRIVATE,
            COLUMN_CREATED_AT,
            COLUMN_UPDATED_AT
    };

    public static void createTable(SQLiteDatabase db) {
        String mQuery = "CREATE TABLE " + TABLE_NAME + " ("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_USER_ID + " INTEGER,"
                + COLUMN_NAME + " TEXT,"
                + COLUMN_DESCRIPTION + " TEXT,"
                + COLUMN_TYPE + " TEXT,"
                + COLUMN_IS_PRIVATE + " INTEGER,"
                + COLUMN_CREATED_AT + " TEXT,"
                + COLUMN_UPDATED_AT + " TEXT);";
        db.execSQL(mQuery);
    }

    public static void dropTable(SQLiteDatabase db) {
        String mQuery = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(mQuery);
    }

}
