package nl.sotallytober.sotallytober.database.migrations;

import android.database.sqlite.SQLiteDatabase;

/**
 * User migration
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class UserMigration {

    public static final String TABLE_NAME = "users";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_INSERTION = "insertion";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_ADMINISTRATOR = "administrator";
    public static final String COLUMN_SESSION_COOKIE = "session_cookie";
    public static final String COLUMN_REMEMBER_ME_COOKIE = "remember_me_cookie";
    public static final String COLUMN_DATE_OF_BIRTH = "date_of_birth";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_UPDATED_AT = "updated_at";

    public static final String[] COLUMNS = {
            COLUMN_ID,
            COLUMN_EMAIL,
            COLUMN_FIRST_NAME,
            COLUMN_INSERTION,
            COLUMN_LAST_NAME,
            COLUMN_ADMINISTRATOR,
            COLUMN_SESSION_COOKIE,
            COLUMN_REMEMBER_ME_COOKIE,
            COLUMN_DATE_OF_BIRTH,
            COLUMN_CREATED_AT,
            COLUMN_UPDATED_AT
    };

    public static void createTable(SQLiteDatabase db) {
        String mQuery = "CREATE TABLE " + TABLE_NAME + " ("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_EMAIL + " TEXT,"
                + COLUMN_FIRST_NAME + " TEXT,"
                + COLUMN_INSERTION + " TEXT,"
                + COLUMN_LAST_NAME + " TEXT,"
                + COLUMN_ADMINISTRATOR + " INTEGER,"
                + COLUMN_SESSION_COOKIE + " TEXT,"
                + COLUMN_REMEMBER_ME_COOKIE + " TEXT,"
                + COLUMN_DATE_OF_BIRTH + " TEXT,"
                + COLUMN_CREATED_AT + " TEXT,"
                + COLUMN_UPDATED_AT + " TEXT);";
        db.execSQL(mQuery);
    }

    public static void dropTable(SQLiteDatabase db) {
        String mQuery = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(mQuery);
    }

}
