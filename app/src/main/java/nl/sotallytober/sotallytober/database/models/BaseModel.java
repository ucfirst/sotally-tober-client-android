package nl.sotallytober.sotallytober.database.models;

/**
 * Application wide model
 *
 * @author tijme
 * @version 1.0.0
 * @date 13-05-15
 */
public class BaseModel {

    protected Integer id;

    @Override
    public boolean equals(Object item) {
        if(((BaseModel) item).getId() == getId()) {
            return true;
        }

        return super.equals(item);
    }

    public boolean existsLocally() {
        return getId() != null;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
