package nl.sotallytober.sotallytober.database.models;

import org.json.JSONException;
import org.json.JSONObject;

import nl.sotallytober.sotallytober.database.providers.UserProvider;

/**
 * Games model
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class GameModel extends BaseModel {

    private String name;

    private Integer user_id;

    private String description;

    private String type;

    private Boolean is_private;

    private String created_at;

    private String updated_at;

    public GameModel() {

    }

    public GameModel(JSONObject game) throws JSONException {
        this(
                game.getInt("id"),
                game.getInt("user_id"),
                game.getString("name"),
                game.getString("description"),
                game.getString("type"),
                game.getInt("private") == 1,
                game.getString("created_at"),
                game.getString("updated_at")
        );
    }

    public GameModel(Integer user_id, String name, String description, String type, Boolean is_private, String created_at, String updated_at) {
        this(null, user_id, name, description, type, is_private, created_at, updated_at);
    }

    public GameModel(Integer id, Integer user_id, String name, String description, String type, Boolean is_private, String created_at, String updated_at) {
        this.setId(id);
        this.setUserId(user_id);
        this.setName(name);
        this.setDescription(description);
        this.setType(type);
        this.setIsPrivate(is_private);
        this.setCreatedAt(created_at);
        this.setUpdatedAt(updated_at);
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserId() {
        return user_id;
    }

    public void setUserId(Integer user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsPrivate() {
        return is_private;
    }

    public void setIsPrivate(Boolean is_private) {
        this.is_private = is_private;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

    public UserModel getOwner() {
        return (UserModel )UserProvider.get().selectFirst(getUserId());
    }

}
