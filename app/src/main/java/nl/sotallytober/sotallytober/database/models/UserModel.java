package nl.sotallytober.sotallytober.database.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * User model
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class UserModel extends BaseModel {

    private String email;

    private String first_name;

    private String insertion;

    private String last_name;

    private boolean administrator;

    private String session_cookie;

    private String remember_me_cookie;

    private String date_of_birth;

    private String created_at;

    private String updated_at;

    public UserModel() {

    }

    public UserModel(JSONObject user) throws JSONException {
        this(user, null, null);
    }

    public UserModel(JSONObject user, String session_cookie, String remember_me_cookie) throws JSONException {
        this(
                user.getInt("id"),
                user.getString("email"),
                user.getString("first_name"),
                user.isNull("insertion") ? "" : user.getString("insertion"),
                user.getString("last_name"),
                user.getInt("administrator") == 1,
                session_cookie,
                remember_me_cookie,
                user.getString("date_of_birth"),
                user.getString("created_at"),
                user.getString("updated_at")
        );
    }

    public UserModel(String email, String first_name, String insertion, String last_name, boolean administrator, String session_cookie, String remember_me_cookie, String date_of_birth, String created_at, String updated_at) {
        this(null, email, first_name, insertion, last_name, administrator, session_cookie, remember_me_cookie, date_of_birth, created_at, updated_at);
    }

    public UserModel(Integer id, String email, String first_name, String insertion, String last_name, boolean administrator, String session_cookie, String remember_me_cookie, String date_of_birth, String created_at, String updated_at) {
        this.setId(id);
        this.setEmail(email);
        this.setFirstName(first_name);
        this.setInsertion(insertion);
        this.setLastName(last_name);
        this.setAdministrator(administrator);
        this.setSessionCookie(session_cookie);
        this.setRememberMeCookie(remember_me_cookie);
        this.setDateOfBirth(date_of_birth);
        this.setCreatedAt(created_at);
        this.setUpdatedAt(updated_at);
    }

    @Override
    public String toString() {
        return getFullName();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getInsertion() {
        return insertion;
    }

    public void setInsertion(String insertion) {
        this.insertion = insertion;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getFullName() {
        String full_name = getFirstName();

        String insertion = getInsertion();
        if(!insertion.isEmpty()) {
            full_name += " " + insertion;
        }

        return full_name + " " + getLastName();
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public String getSessionCookie() {
        return session_cookie;
    }

    public void setSessionCookie(String session_cookie) {
        this.session_cookie = session_cookie;
    }

    public String getRememberMeCookie() {
        return remember_me_cookie;
    }

    public void setRememberMeCookie(String remember_me_cookie) {
        this.remember_me_cookie = remember_me_cookie;
    }

    public String getDateOfBirth() {
        return date_of_birth;
    }

    public void setDateOfBirth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

}
