package nl.sotallytober.sotallytober.database.providers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import nl.sotallytober.sotallytober.database.DatabaseHandler;
import nl.sotallytober.sotallytober.database.models.BaseModel;

/**
 * Application wide provider
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
abstract public class BaseProvider {

    abstract protected BaseModel cursorToModel(Cursor cursor);

    abstract protected ContentValues modelToContentValues(BaseModel model);

    abstract protected String getTable();

    abstract protected String getPrimaryColumn();

    protected ArrayList<BaseModel> cursorToArrayList(Cursor cursor) {
        ArrayList<BaseModel> rows = new ArrayList<BaseModel>();

        while (cursor.moveToNext()) {
            rows.add(cursorToModel(cursor));
        }

        return rows;
    }

    public BaseModel selectFirst(Integer primaryValue) {
        String mQuery = getPrimaryColumn() + " = ?";
        String[] mQueryArgs = new String[] {
                String.valueOf(primaryValue)
        };

        return selectFirst(mQuery, mQueryArgs);
    }

    public BaseModel selectFirst(String mQuery, String[] mQueryArgs) {
        SQLiteDatabase db = DatabaseHandler.get().getReadableDatabase();
        Cursor cursor = db.query(getTable(), null, mQuery, mQueryArgs, null, null, null, "1");

        if(cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            return cursorToModel(cursor);
        }

        return null;
    }

    public ArrayList<BaseModel> selectAll() {
        return selectAll(null, null);
    }

    public ArrayList<BaseModel> selectAll(String mQuery, String[] mQueryArgs) {
        SQLiteDatabase db = DatabaseHandler.get().getReadableDatabase();
        Cursor cursor = db.query(getTable(), null, mQuery, mQueryArgs, null, null, null, null);
        return cursorToArrayList(cursor);
    }

    public BaseModel insert(BaseModel row) {
        SQLiteDatabase db = DatabaseHandler.get().getWritableDatabase();
        long result = db.insert(getTable(), null, modelToContentValues(row));

        if(result < 0) {
            throw new SQLiteException("Insert statement failed");
        } else {
            int id = (int) result;
            row.setId(id);
            return row;
        }
    }

    public BaseModel insertIfNotExists(BaseModel row) {
        if(row.getId() == null) {
            return insert(row);
        }

        if(selectFirst(row.getId()) == null) {
            return insert(row);
        }

        return null;
    }

    public BaseModel insertOrUpdate(BaseModel row) {
        BaseModel inserted = insertIfNotExists(row);
        if(inserted != null) {
            return inserted;
        }

        return update(row);
    }

    public BaseModel update(BaseModel row) {
        SQLiteDatabase db = DatabaseHandler.get().getWritableDatabase();
        long rowsAffected = db.update(
                getTable(),
                modelToContentValues(row),
                getPrimaryColumn() + " = ?",
                new String[] { String.valueOf(row.getId()) }
        );

        if(rowsAffected < 0) {
            return null;
        }

        return row;
    }

    public boolean delete(BaseModel row) {
        return delete(row.getId());
    }

    public boolean delete(int id) {
        return delete(getPrimaryColumn() + " = ?", new String[]{ String.valueOf(id) });
    }

    public boolean delete(String mQuery, String[] mQueryArgs) {
        SQLiteDatabase db = DatabaseHandler.get().getWritableDatabase();
        return db.delete(getTable(), mQuery, mQueryArgs) > 0;
    }

    public boolean deleteAll() {
        SQLiteDatabase db = DatabaseHandler.get().getWritableDatabase();
        return db.delete(getTable(), null, null) > 0;
    }

}
