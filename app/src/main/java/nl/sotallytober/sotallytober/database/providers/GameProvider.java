package nl.sotallytober.sotallytober.database.providers;

import android.content.ContentValues;
import android.database.Cursor;

import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.models.BaseModel;

/**
 * Game provider
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class GameProvider extends BaseProvider {

    private static GameProvider mInstance = null;

    public static GameProvider get() {
        if(mInstance == null) {
            mInstance = new GameProvider();
        }

        return mInstance;
    }

    @Override
    public String getTable() {
        return GameMigration.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumn() {
        return GameMigration.COLUMN_ID;
    }

    @Override
    public BaseModel cursorToModel(Cursor cursor) {
        if(cursor == null) {
            return null;
        }

        GameModel game = new GameModel();
        game.setId(cursor.getInt(cursor.getColumnIndex(GameMigration.COLUMN_ID)));
        game.setUserId(cursor.getInt(cursor.getColumnIndex(GameMigration.COLUMN_USER_ID)));
        game.setName(cursor.getString(cursor.getColumnIndex(GameMigration.COLUMN_NAME)));
        game.setDescription(cursor.getString(cursor.getColumnIndex(
                GameMigration.COLUMN_DESCRIPTION)));
        game.setType(cursor.getString(cursor.getColumnIndex(GameMigration.COLUMN_TYPE)));
        game.setCreatedAt(cursor.getString(cursor.getColumnIndex(GameMigration.COLUMN_CREATED_AT)));
        game.setUpdatedAt(cursor.getString(cursor.getColumnIndex(GameMigration.COLUMN_UPDATED_AT)));
        return game;
    }

    @Override
    public ContentValues modelToContentValues(BaseModel model) {
        GameModel game = (GameModel) model;
        ContentValues values = new ContentValues();

        if(game.existsLocally()) {
            values.put(GameMigration.COLUMN_ID, game.getId());
        }

        values.put(GameMigration.COLUMN_USER_ID, game.getUserId());
        values.put(GameMigration.COLUMN_NAME, game.getName());
        values.put(GameMigration.COLUMN_DESCRIPTION, game.getDescription());
        values.put(GameMigration.COLUMN_TYPE, game.getType());
        values.put(GameMigration.COLUMN_CREATED_AT, game.getCreatedAt());
        values.put(GameMigration.COLUMN_UPDATED_AT, game.getUpdatedAt());

        return values;
    }

}
