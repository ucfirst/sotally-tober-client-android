package nl.sotallytober.sotallytober.database.providers;

import android.content.ContentValues;
import android.database.Cursor;

import nl.sotallytober.sotallytober.database.migrations.UserMigration;
import nl.sotallytober.sotallytober.database.models.BaseModel;
import nl.sotallytober.sotallytober.database.models.UserModel;

/**
 * User provider
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class UserProvider extends BaseProvider {

    private static UserProvider mInstance = null;

    public static UserProvider get() {
        if(mInstance == null) {
            mInstance = new UserProvider();
        }

        return mInstance;
    }

    @Override
    public String getTable() {
        return UserMigration.TABLE_NAME;
    }

    @Override
    public String getPrimaryColumn() {
        return UserMigration.COLUMN_ID;
    }

    @Override
    public BaseModel cursorToModel(Cursor cursor) {
        if(cursor == null) {
            return null;
        }

        UserModel user = new UserModel();
        user.setId(cursor.getInt(cursor.getColumnIndex(UserMigration.COLUMN_ID)));
        user.setEmail(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_EMAIL)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_FIRST_NAME)));
        user.setInsertion(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_INSERTION)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_LAST_NAME)));
        user.setAdministrator(cursor.getInt(cursor.getColumnIndex(
                UserMigration.COLUMN_ADMINISTRATOR)) > 0);
        user.setSessionCookie(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_SESSION_COOKIE)));
        user.setRememberMeCookie(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_SESSION_COOKIE)));
        user.setDateOfBirth(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_DATE_OF_BIRTH)));
        user.setCreatedAt(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_CREATED_AT)));
        user.setUpdatedAt(cursor.getString(cursor.getColumnIndex(UserMigration.COLUMN_UPDATED_AT)));
        return user;
    }

    @Override
    public ContentValues modelToContentValues(BaseModel model) {
        UserModel user = (UserModel) model;
        ContentValues values = new ContentValues();

        if(user.existsLocally()) {
            values.put(UserMigration.COLUMN_ID, user.getId());
        }

        values.put(UserMigration.COLUMN_EMAIL, user.getEmail());
        values.put(UserMigration.COLUMN_FIRST_NAME, user.getFirstName());
        values.put(UserMigration.COLUMN_INSERTION, user.getInsertion());
        values.put(UserMigration.COLUMN_LAST_NAME, user.getLastName());
        values.put(UserMigration.COLUMN_ADMINISTRATOR, user.isAdministrator());
        values.put(UserMigration.COLUMN_SESSION_COOKIE, user.getSessionCookie());
        values.put(UserMigration.COLUMN_REMEMBER_ME_COOKIE, user.getRememberMeCookie());
        values.put(UserMigration.COLUMN_DATE_OF_BIRTH, user.getDateOfBirth());
        values.put(UserMigration.COLUMN_CREATED_AT, user.getCreatedAt());
        values.put(UserMigration.COLUMN_UPDATED_AT, user.getUpdatedAt());

        return values;
    }

}
