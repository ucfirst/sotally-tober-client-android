package nl.sotallytober.sotallytober.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.activities.MainActivity;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;
import nl.sotallytober.sotallytober.tasks.LogoutUserAsyncTask;

/**
 * Account fragment
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class AccountFragment extends Fragment implements LogoutUserAsyncTask.OnUserLogoutAsyncTaskListener {

    private LogoutUserAsyncTask mAuthenticationTask = null;

    private ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserModel user = Auth.getUser();

        TextView welcome = (TextView) view.findViewById(R.id.fragment_account_welcome);
        welcome.setText(getString(R.string.fragment_account_welcome, user.getFirstName()));

        TextView fullname = (TextView) view.findViewById(R.id.fragment_account_fullname);
        fullname.setText(user.getFullName());

        TextView email = (TextView) view.findViewById(R.id.fragment_account_email);
        email.setText(user.getEmail());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_account_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_logout:
                mAuthenticationTask = new LogoutUserAsyncTask(this);
                mAuthenticationTask.execute();

                dialog = new ProgressDialog(getActivity());
                dialog.setTitle(getString(R.string.status_logging_out));
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(R.string.activity_main_menu_account);
    }

    @Override
    public void onUserLogoutAsyncTaskSuccess(Response mResponse) {
        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(getActivity(), mResponse.getMessage(getActivity().getApplicationContext()), Toast.LENGTH_LONG).show();
        }

        dialog.dismiss();
        ((MainActivity) getActivity()).logoutAndShowLogin();
    }

    @Override
    public void onUserLogoutAsyncTaskFailed(Response mResponse) {
        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(getActivity(), mResponse.getMessage(getActivity().getApplicationContext()), Toast.LENGTH_LONG).show();
        }

        dialog.dismiss();
        ((MainActivity) getActivity()).logoutAndShowLogin();
    }

}
