package nl.sotallytober.sotallytober.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.activities.MainActivity;

/**
 * Challenges fragment
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class ChallengesFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_challenges, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(R.string.activity_main_menu_challenges);
    }

}
