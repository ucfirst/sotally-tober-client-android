package nl.sotallytober.sotallytober.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;

/**
 * DatePicker fragment
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 01-05-15
 */
public class DatePickerFragment extends DialogFragment {

    DatePickerDialog.OnDateSetListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int year = (c.get(Calendar.YEAR) - 18);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        String date = getArguments().getString("date");
        if(date != null && date.length() > 0) {
            String[] exploded = date.split("-");
            year = Integer.parseInt(exploded[0]);
            month = Integer.parseInt(exploded[1]) - 1;
            day = Integer.parseInt(exploded[2]);
        }

        return new DatePickerDialog(getActivity(), mListener, year, month, day);
    }

    @Override
    public void onAttach(Activity activity) {
        mListener = (DatePickerDialog.OnDateSetListener) activity;
        super.onAttach(activity);
    }

}