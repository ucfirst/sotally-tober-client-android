package nl.sotallytober.sotallytober.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.activities.CreateOrUpdateGameActivity;
import nl.sotallytober.sotallytober.comparators.GamesComparator;
import nl.sotallytober.sotallytober.database.migrations.GameMigration;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.models.BaseModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;
import nl.sotallytober.sotallytober.helpers.LogHelper;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;
import nl.sotallytober.sotallytober.tasks.DeleteGameAsyncTask;
import nl.sotallytober.sotallytober.tasks.IndexGamesAsyncTask;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import nl.sotallytober.sotallytober.activities.MainActivity;
import nl.sotallytober.sotallytober.adapters.GameAdapter;

/**
 * List fragment
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 04-05-15
 */
public class GamesFragment extends ListFragment implements AdapterView.OnItemLongClickListener,
        IndexGamesAsyncTask.OnIndexGamesAsyncTaskListener, SwipeRefreshLayout.OnRefreshListener,
        AbsListView.OnScrollListener, DeleteGameAsyncTask.OnDeleteGameAsyncTaskListener {

    private final int MENU_ACTION_EDIT = 0;

    private final int MENU_ACTION_EDIT_TASKS = 1;

    private final int MENU_ACTION_DELETE = 2;

    private final int MENU_ACTION_DELETE_FROM_FAVORITES = 3;

    private GameAdapter listAdapter;

    private SwipeRefreshLayout swipeLayout;

    private IndexGamesAsyncTask mIndexGamesAsyncTask;

    private ArrayList<BaseModel> listItems = new ArrayList<BaseModel>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listItems.addAll(GameProvider.get().selectAll());
        Collections.sort(listItems, new GamesComparator());
        listAdapter = new GameAdapter(getActivity(), listItems);
        setListAdapter(listAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_games_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_add:
                Intent editGameIntent = new Intent(getActivity(), CreateOrUpdateGameActivity.class);
                startActivityForResult(editGameIntent, CreateOrUpdateGameActivity.REQUEST_CODE);
                return true;
            case R.id.menu_action_help:
                Toast.makeText(getActivity(), "MENU ACTION HELP", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(R.string.activity_main_menu_games);
    }

    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        registerForContextMenu(getListView());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_games, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.game_swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        if(listItems.isEmpty()) {
            executeIndexGamesAsyncTask();
        }

        getListView().setOnItemLongClickListener(this);
        getListView().setOnScrollListener(this);
    }

    @Override
    public void onRefresh() {
        executeIndexGamesAsyncTask();
    }

    private void executeIndexGamesAsyncTask() {
        if (mIndexGamesAsyncTask == null) {
            mIndexGamesAsyncTask = new IndexGamesAsyncTask(this);
            mIndexGamesAsyncTask.execute();
        } else {
            mIndexGamesAsyncTask.cancel(true);
        }
    }

    @Override
    public void onIndexGamesAsyncTaskSuccess(ArrayList<GameModel> rows) {
        for(GameModel row : rows) {
            if(!listItems.contains(row)) {
                listItems.add(row);
            }
        }

        Collections.sort(listItems, new GamesComparator());
        listAdapter.notifyDataSetChanged();
        mIndexGamesAsyncTask = null;
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onIndexGamesAsyncTaskFailed(Response mResponse) {
        if(mResponse.getStatus().equals(Response.STATUS_ERROR_MESSAGE)) {
            Toast.makeText(getActivity(), mResponse.getMessage(getActivity().getApplicationContext()), Toast.LENGTH_LONG).show();
        }

        mIndexGamesAsyncTask = null;
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onIndexGamesAsyncTaskCancelled() {
        mIndexGamesAsyncTask = null;
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Toast.makeText(getActivity(), "SHORT:" + v.toString() + " pos:" + position + " id:" + id, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        final int position = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        final GameModel item = (GameModel) listAdapter.getItem(position);

        menu.setHeaderTitle(item.toString());

        if(item.getUserId().equals(Auth.getUser().getId())) {
            menu.add(0, MENU_ACTION_EDIT, 0, R.string.fragment_games_action_edit_game);

            switch(item.getType()) {
                case GameMigration.TYPE_TASK:
                    menu.add(0, MENU_ACTION_EDIT_TASKS, 0, R.string.fragment_games_action_edit_game_taks);
                    break;
            }

            menu.add(0, MENU_ACTION_DELETE, 0, R.string.fragment_games_action_delete_game);
        } else {
            menu.add(0, MENU_ACTION_DELETE_FROM_FAVORITES, 0, R.string.fragment_games_action_delete_game_from_favorites);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        final GameModel clickedList = (GameModel) listAdapter.getItem(position);

        switch (item.getItemId()) {
            case MENU_ACTION_EDIT:
                Intent editGameIntent = new Intent(getActivity(), CreateOrUpdateGameActivity.class);
                editGameIntent.putExtra(GameMigration.COLUMN_ID, clickedList.getId());
                startActivityForResult(editGameIntent, CreateOrUpdateGameActivity.REQUEST_CODE);
                return true;
            case MENU_ACTION_EDIT_TASKS:
//                Intent editGameIntent = new Intent(getActivity(), CreateOrUpdateGameActivity.class);
//                editGameIntent.putExtra(GameMigration.COLUMN_ID, clickedList.getId());
//                startActivityForResult(editGameIntent, CreateOrUpdateGameActivity.REQUEST_CODE);
                return true;
            case MENU_ACTION_DELETE:
            case MENU_ACTION_DELETE_FROM_FAVORITES:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                new DeleteGameAsyncTask(GamesFragment.this, clickedList).execute();
                                listItems.remove(position);
                                listAdapter.notifyDataSetChanged();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.question_are_you_sure))
                        .setPositiveButton(getString(R.string.yes), dialogClickListener)
                        .setNegativeButton(getString(R.string.cancel), dialogClickListener)
                        .show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CreateOrUpdateGameActivity.REQUEST_CODE) {
            switch(resultCode) {
                case CreateOrUpdateGameActivity.RESULT_CREATE_COMPLETED:
                    int createdId = data.getExtras().getInt(GameMigration.COLUMN_ID);
                    GameModel createdGame = (GameModel) GameProvider.get().selectFirst(createdId);
                    listItems.add(createdGame);
                    break;
                case CreateOrUpdateGameActivity.RESULT_UPDATE_COMPLETED:
                    int updatedId = data.getExtras().getInt(GameMigration.COLUMN_ID);
                    GameModel updatedGame = (GameModel) GameProvider.get().selectFirst(updatedId);
                    listItems.remove(updatedGame);
                    listItems.add(updatedGame);
                    break;
                case CreateOrUpdateGameActivity.RESULT_CANCELLED:
                    LogHelper.debug("Game not saved because user cancelled request.");
                    break;
            }

            Collections.sort(listItems, new GamesComparator());
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int position = getListView().getChildCount() == 0 ? 0 : getListView().getChildAt(0).getTop();
        swipeLayout.setEnabled(firstVisibleItem == 0 && position >= 0);
    }

    @Override
    public void onDeleteGameAsyncTaskFailed(GameModel mOldRow) {
        listItems.add(mOldRow);
        Collections.sort(listItems, new GamesComparator());
        listAdapter.notifyDataSetChanged();
        Toast.makeText(getActivity(), getString(R.string.fragment_account_error_could_not_delete_game, mOldRow.toString()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteGameAsyncTaskCancelled(GameModel mOldRow) {
        listItems.add(mOldRow);
        Collections.sort(listItems, new GamesComparator());
        listAdapter.notifyDataSetChanged();
    }

}
