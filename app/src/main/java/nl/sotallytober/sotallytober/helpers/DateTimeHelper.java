package nl.sotallytober.sotallytober.helpers;

/**
 * DateTime utility class
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 18-05-15
 */
public class DateTimeHelper {

    private DateTimeHelper() {

    }

    public static String getSQLDateTime() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        return new java.sql.Timestamp(calendar.getTime().getTime()).toString();
    }

}
