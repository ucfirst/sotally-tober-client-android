package nl.sotallytober.sotallytober.helpers;

import android.util.Log;
import org.json.JSONObject;

import nl.sotallytober.sotallytober.Configuration;

/**
 * Handles log requests
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 23-05-15
 */
public class LogHelper {

    public static final String TAG = Configuration.APP_SLUG + "Log";

    public static void debug(String message) {
        if(Configuration.DEBUG) {
            Log.d(TAG, message);
        }
    }

    public static void error(String message) {
        if(Configuration.DEBUG) {
            Log.e(TAG, message);
        }
    }

    public static void printJSON(JSONObject json) {
        if(Configuration.DEBUG_HTTP) {
            Log.d(TAG, json.toString());
        }
    }

    public static void printStackTrace(Exception e) {
        if(Configuration.DEBUG) {
            System.out.println("\n Exception: " + e.getMessage());
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

}
