package nl.sotallytober.sotallytober.http;

import android.os.Build;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import nl.sotallytober.sotallytober.BuildConfig;
import nl.sotallytober.sotallytober.Configuration;
import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.activities.MainActivity;
import nl.sotallytober.sotallytober.database.providers.UserProvider;
import nl.sotallytober.sotallytober.helpers.LogHelper;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;

/**
 * Connection handles HTTP(S) connections
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 22-05-15
 */
public class Connection {

    public static final String REQUEST_METHOD_GET = "GET";

    public static final String REQUEST_METHOD_POST = "POST";

    private static final String URL_ENCODING = "UTF-8";

    private static final int RESPONSE_CODE_OK = 200;

    private static final int RESPONSE_CODE_UNAUTHORIZED = 401;

    private final String mRequestUrl;

    private final String mRequestMethod;

    private final HashMap<String, String> mRequestParameters;

    public Connection(final String mRequestUrl, final String mRequestMethod) {
        this(mRequestUrl, mRequestMethod, null);
    }

    public Connection(final String mRequestUrl, final String mRequestMethod, final HashMap<String, String> mRequestParameters) {
        this.mRequestUrl = mRequestUrl;
        this.mRequestMethod = mRequestMethod;
        this.mRequestParameters = mRequestParameters;
    }

    public Response getResponse() {
        // Get API URL for current action
        URL url;
        try {
            url = getAbsoluteURL();
        } catch (MalformedURLException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_malformed_url_exception);
        }

        // Get connection for current call
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_io_exception);
        }

        // Set request variables
        try {
            connection.setRequestMethod(mRequestMethod);
            connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
        } catch (ProtocolException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_protocol_exception);
        }

        // Set user agent
        connection.setRequestProperty("User-Agent", getUserAgent());

        // Set cookie
        connection.setRequestProperty("Cookie", getUserCookie());

        // Set POST data
        if(mRequestMethod.equals(REQUEST_METHOD_POST) && !mRequestParameters.isEmpty()) {
            try {
                connection.setDoOutput(true);
                DataOutputStream output = new DataOutputStream(connection.getOutputStream());
                output.writeBytes(getParameters());
                output.flush();
                output.close();
            } catch (UnsupportedEncodingException e) {
                LogHelper.printStackTrace(e);
                return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_unsupported_encoding_exception);
            } catch (IOException e) {
                LogHelper.printStackTrace(e);
                return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_io_exception);
            }
        }

        // Get response code
        int responseCode;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_io_exception);
        }

        // Check response code
        if(responseCode != RESPONSE_CODE_OK) {
            LogHelper.debug("Got different response code. Response code: " + responseCode);

            if(responseCode == RESPONSE_CODE_UNAUTHORIZED) {
                return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_session_expired);
            } else {
                return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_wrong_response_code);
            }
        }

        // Get input stream
        InputStream input;
        try {
            input = connection.getInputStream();
        } catch (IOException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_io_exception);
        }

        // Get output
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        StringBuffer response = new StringBuffer();
        String readerLine = null;

        try {
            while((readerLine = reader.readLine()) != null) {
                response.append(readerLine);
            }
            reader.close();
        } catch (IOException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_io_exception);
        }

        // Update user session
        String session = getCookie(connection, Configuration.API_COOKIE_SESSION_KEY);
        String rememberMe = getCookie(connection, Configuration.API_COOKIE_REMEMBER_ME_PREFIX);

        if(Auth.isLoggedIn()) {
            if(session != null && session.length() > 0) Auth.getUser().setSessionCookie(session);
            if(rememberMe != null && rememberMe.length() > 0) Auth.getUser().setRememberMeCookie(rememberMe);
            UserProvider.get().update(Auth.getUser());
        }

        return new Converter(response.toString(), session, rememberMe).getResponse();
    }

    private String getCookie(HttpURLConnection connection, String requestedCookie) {
        List<String> cookies = connection.getHeaderFields().get("Set-Cookie");

        if(cookies != null) {
            for(String cookie : cookies) {
                HttpCookie httpCookie = HttpCookie.parse(cookie).get(0);
                if(httpCookie.getName().startsWith(requestedCookie)) {
                    return httpCookie.getName() + "=" + httpCookie.getValue();
                }
            }
        }

        return "";
    }

    private URL getAbsoluteURL() throws MalformedURLException {
        String base = "http://" + Configuration.API_BASE_URL;
        String version = "/v" + Configuration.API_VERSION;
        return new URL(base + version + mRequestUrl);
    }

    private String getUserAgent() {
        return Configuration.APP_SLUG + "/" + BuildConfig.VERSION_CODE + " " + "Android" + "/" + Build.VERSION.RELEASE;
    }

    private String getUserCookie() {
        if(Auth.isLoggedIn()) {
            String session = Auth.getUser().getSessionCookie();
            String rememberMe = Auth.getUser().getRememberMeCookie();
            return session + "; " + rememberMe;
        }

        return "";
    }

    private String getParameters() throws UnsupportedEncodingException {
        StringBuilder query = new StringBuilder();

        for(HashMap.Entry<String, String> e : mRequestParameters.entrySet()) {
            String key = URLEncoder.encode(e.getKey(), URL_ENCODING);
            String value = URLEncoder.encode(e.getValue(), URL_ENCODING);
            query.append('&').append(key).append("=").append(value);
        }

        return query.toString().substring(1);
    }

}
