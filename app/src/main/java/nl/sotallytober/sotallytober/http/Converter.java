package nl.sotallytober.sotallytober.http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.helpers.LogHelper;

/**
 * Converts a Connection into Response
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 23-05-15
 */
public class Converter {

    private final static String JSON_STATUS_SUCCESS = "success";

    private final static String JSON_STATUS_FAIL = "fail";

    private final String mConnectionResponse;

    private final String mSessionCookie;

    private final String mRememberMeCookie;

    public Converter(final String mConnectionResponse, final String mSessionCookie, final String mRememberMeCookie) {
        this.mConnectionResponse = mConnectionResponse;
        this.mSessionCookie = mSessionCookie;
        this.mRememberMeCookie = mRememberMeCookie;
    }

    public Response getResponse() {
        JSONObject root;
        String status = null;
        String message = null;
        String data = null;

        try {
            root = new JSONObject(mConnectionResponse);
            LogHelper.printJSON(root);

            if(root.has("status")) status = root.getString("status");
            if(root.has("message")) message = root.getString("message");
            if(root.has("data")) data = root.getString("data");
        } catch (JSONException e) {
            LogHelper.printStackTrace(e);
            LogHelper.debug(mConnectionResponse);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_json_exception);
        }

        Response validResponse = new Response();
        validResponse.setSessionCookie(mSessionCookie);
        validResponse.setRememberMeCookie(mRememberMeCookie);

        try {
            if(status.equals(JSON_STATUS_SUCCESS)) {
                validResponse.setStatus(Response.STATUS_SUCCESS);
                validResponse.setData(data);
            } else if(status.equals(JSON_STATUS_FAIL) && message != null) {
                validResponse.setStatus(Response.STATUS_ERROR_MESSAGE);
                validResponse.setMessage(message);
            } else if(status.equals(JSON_STATUS_FAIL) && data != null) {
                validResponse.setStatus(Response.STATUS_ERROR_VALIDATION);
                validResponse.setValidation(getValidationFromString(data));
            } else {
                return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_invalid_json_structure);
            }
        } catch (JSONException e) {
            LogHelper.printStackTrace(e);
            return new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_json_exception);
        }

        return validResponse;
    }

    private HashMap<String, ArrayList<String>> getValidationFromString(String root) throws JSONException {
        JSONObject data = new JSONObject(root.substring(root.indexOf("{"), root.lastIndexOf("}") + 1));
        HashMap<String, ArrayList<String>> validation = new HashMap<String, ArrayList<String>>();
        Iterator<?> dataKeys = data.keys();

        while(dataKeys.hasNext()) {
            String dataKey = (String) dataKeys.next();
            if(data.get(dataKey) instanceof JSONArray) {
                JSONArray jsonMessages = (JSONArray) data.get(dataKey);
                ArrayList<String> validationMessages = new ArrayList<>();
                for(int i = 0; i < jsonMessages.length(); i++) {
                    validationMessages.add(jsonMessages.getString(i));
                }
                validation.put(dataKey, validationMessages);
            }
        }

        return validation;
    }

}
