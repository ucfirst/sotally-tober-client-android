package nl.sotallytober.sotallytober.http;

import android.content.Context;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nl.sotallytober.sotallytober.R;

/**
 * HTTP(S) response class
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 22-05-15
 */
public class Response {

    public static final String STATUS_SUCCESS = "success";

    public static final String STATUS_ERROR_VALIDATION = "error_validation";

    public static final String STATUS_ERROR_MESSAGE = "error_message";

    private String mStatus;

    private String mMessage;

    private Integer mMessageResource;

    private String mData;

    private String mSessionCookie;

    private String mRememberMeCookie;

    private HashMap<String, ArrayList<String>> mValidation;

    public Response() {

    }

    public Response(final String mStatus, final int mMessage) {
        setStatus(mStatus);
        setMessage(mMessage);
    }

    public Response(final String mStatus, final String mMessage) {
        setStatus(mStatus);
        setMessage(mMessage);
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getMessage(Context context) {
        if(mMessage != null && mMessage.length() > 0) {
            return mMessage;
        }

        return context.getString(mMessageResource);
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public void setMessage(Integer mMessageResource) {
        this.mMessageResource = mMessageResource;
    }

    public String getData() {
        return mData;
    }

    public void setData(String mData) {
        this.mData = mData;
    }

    public String getSessionCookie() {
        return mSessionCookie;
    }

    public void setSessionCookie(String mSessionCookie) {
        this.mSessionCookie = mSessionCookie;
    }

    public String getRememberMeCookie() {
        return mRememberMeCookie;
    }

    public void setRememberMeCookie(String mRememberMeCookie) {
        this.mRememberMeCookie = mRememberMeCookie;
    }

    public HashMap<String, ArrayList<String>> getValidation() {
        return mValidation;
    }

    public void setValidation(HashMap<String, ArrayList<String>> mValidation) {
        this.mValidation = mValidation;
    }

}
