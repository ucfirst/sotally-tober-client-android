package nl.sotallytober.sotallytober.libraries.authentication;

import nl.sotallytober.sotallytober.database.DatabaseHandler;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.providers.UserProvider;

/**
 * Authentication
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 02-05-15
 */
public class Auth {

    private static UserModel sUser;

    private Auth() {

    }

    public static void sync() {
        sUser = (UserModel) UserProvider.get().selectFirst(null, null);
    }

    public static void setUser(UserModel loginUser) {
        UserProvider.get().insertIfNotExists(loginUser);
        sUser = loginUser;
    }

    public static UserModel getUser() {
        return sUser;
    }

    public static boolean isLoggedIn() {
        return sUser != null && sUser.existsLocally();
    }

    public static void logout() {
        DatabaseHandler.get().emptyDatabase();
        sUser = null;
    }

}
