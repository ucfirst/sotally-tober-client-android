package nl.sotallytober.sotallytober.tasks;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;
import nl.sotallytober.sotallytober.database.providers.UserProvider;
import nl.sotallytober.sotallytober.helpers.LogHelper;
import nl.sotallytober.sotallytober.http.Connection;
import nl.sotallytober.sotallytober.http.Response;

/**
 * Creates a GameModel
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class CreateGameAsyncTask extends AsyncTask<Void, Void, GameModel> {

    private final OnCreateGameAsyncTaskListener mCallback;

    private final String mRequestURL = "/game/create";

    private final HashMap<String, String> mGameDetails = new HashMap<>();

    private Response mResponse;

    public CreateGameAsyncTask(OnCreateGameAsyncTaskListener mCallback, GameModel game) {
        this.mCallback = mCallback;

        mGameDetails.put("name", game.getName());
        mGameDetails.put("description", game.getDescription());
        mGameDetails.put("type", game.getType());
        mGameDetails.put("private", (game.getIsPrivate() ? "1" : "0"));
    }

    public interface OnCreateGameAsyncTaskListener {
        void onCreateGameAsyncTaskCompleted(GameModel newGame);
        void onCreateGameAsyncTaskFailed(Response mResponse);
    }

    @Override
    protected GameModel doInBackground(Void... params) {
        mResponse = new Connection(mRequestURL, Connection.REQUEST_METHOD_POST, mGameDetails).getResponse();

        if(!mResponse.getStatus().equals(Response.STATUS_SUCCESS)) {
            return null;
        }

        GameModel gameModel;
        try {
            JSONObject game = new JSONObject(mResponse.getData());
            gameModel = new GameModel(game);
        } catch (JSONException e) {
            LogHelper.printStackTrace(e);
            mResponse = new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_json_exception);
            return null;
        }

        GameProvider.get().insertOrUpdate(gameModel);

        return gameModel;
    }

    @Override
    protected void onPostExecute(GameModel game) {
        super.onPostExecute(game);

        if(game != null) {
            mCallback.onCreateGameAsyncTaskCompleted(game);
        } else {
            mCallback.onCreateGameAsyncTaskFailed(mResponse);
        }
    }

}
