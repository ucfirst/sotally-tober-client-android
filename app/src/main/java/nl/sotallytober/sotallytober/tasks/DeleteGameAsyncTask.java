package nl.sotallytober.sotallytober.tasks;

import android.os.AsyncTask;

import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;

/**
 * Deletes a GameModel
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class DeleteGameAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final OnDeleteGameAsyncTaskListener mCallback;

    private final GameModel mGame;

    public DeleteGameAsyncTask(OnDeleteGameAsyncTaskListener mCallback, GameModel mGame) {
        this.mCallback = mCallback;
        this.mGame = mGame;
    }

    public interface OnDeleteGameAsyncTaskListener {
        public void onDeleteGameAsyncTaskFailed(GameModel mOldGame);
        public void onDeleteGameAsyncTaskCancelled(GameModel mOldGame);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            Thread.sleep(2000);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);

        if(success) {
            GameProvider.get().delete(mGame);
        } else {
            mCallback.onDeleteGameAsyncTaskFailed(mGame);
        }
    }

    @Override
    protected void onCancelled() {
        mCallback.onDeleteGameAsyncTaskCancelled(mGame);
    }

}
