package nl.sotallytober.sotallytober.tasks;

import android.content.res.Resources;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;
import nl.sotallytober.sotallytober.database.providers.UserProvider;
import nl.sotallytober.sotallytober.helpers.LogHelper;
import nl.sotallytober.sotallytober.http.Connection;
import nl.sotallytober.sotallytober.http.Response;
import nl.sotallytober.sotallytober.libraries.authentication.Auth;

/**
 * Retrieves all GameModels
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class IndexGamesAsyncTask extends AsyncTask<Void, Void, ArrayList<GameModel>> {

    private final String mRequestURL = "/game";

    private Response mResponse;

    private final OnIndexGamesAsyncTaskListener mCallback;

    private final ArrayList<GameModel> mItems = new ArrayList<>();

    public IndexGamesAsyncTask(OnIndexGamesAsyncTaskListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnIndexGamesAsyncTaskListener {
        void onIndexGamesAsyncTaskSuccess(ArrayList<GameModel> rows);
        void onIndexGamesAsyncTaskFailed(Response mResponse);
        void onIndexGamesAsyncTaskCancelled();
    }

    @Override
    protected ArrayList<GameModel> doInBackground(Void... params) {
        mResponse = new Connection(mRequestURL, Connection.REQUEST_METHOD_GET).getResponse();

        if(!mResponse.getStatus().equals(Response.STATUS_SUCCESS)) {
            return null;
        }

        try {
            JSONArray games = new JSONArray(mResponse.getData());
            for (int i = 0; i < games.length(); i++) {
                JSONObject game = games.getJSONObject(i);

                // Parse and save and add game
                GameModel gameModel = new GameModel(game);
                GameProvider.get().insertOrUpdate(gameModel);
                mItems.add(gameModel);

                // Parse and save owner
                if(game.has("owner")) {
                    UserModel userModel = new UserModel(game.getJSONObject("owner"));
                    if(!userModel.getId().equals(Auth.getUser().getId())) {
                        UserProvider.get().insertOrUpdate(userModel);
                    }
                }
            }
        } catch (JSONException e) {
            LogHelper.printStackTrace(e);
            mResponse = new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_json_exception);
            return null;
        }

        return mItems;
    }

    @Override
    protected void onPostExecute(ArrayList<GameModel> rows) {
        super.onPostExecute(rows);

        if(rows == null || rows.isEmpty()) {
            mCallback.onIndexGamesAsyncTaskFailed(mResponse);
        } else {
            mCallback.onIndexGamesAsyncTaskSuccess(rows);
        }
    }

    @Override
    protected void onCancelled() {
        mCallback.onIndexGamesAsyncTaskCancelled();
    }

}
