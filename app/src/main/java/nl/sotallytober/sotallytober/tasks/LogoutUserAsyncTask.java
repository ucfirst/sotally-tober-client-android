package nl.sotallytober.sotallytober.tasks;

import android.os.AsyncTask;

import nl.sotallytober.sotallytober.http.Connection;
import nl.sotallytober.sotallytober.http.Response;

/**
 * Tries to logout a UserModel
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class LogoutUserAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final OnUserLogoutAsyncTaskListener mCallback;

    private final String mRequestURL = "/auth/logout";

    private Response mResponse;

    public LogoutUserAsyncTask(OnUserLogoutAsyncTaskListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnUserLogoutAsyncTaskListener {
        void onUserLogoutAsyncTaskSuccess(Response mResponse);
        void onUserLogoutAsyncTaskFailed(Response mResponse);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        mResponse = new Connection(mRequestURL, Connection.REQUEST_METHOD_GET).getResponse();

        return mResponse.getStatus().equals(Response.STATUS_SUCCESS);
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);

        if(success) {
            mCallback.onUserLogoutAsyncTaskSuccess(mResponse);
        } else {
            mCallback.onUserLogoutAsyncTaskFailed(mResponse);
        }
    }

}
