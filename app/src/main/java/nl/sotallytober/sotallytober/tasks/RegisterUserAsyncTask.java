package nl.sotallytober.sotallytober.tasks;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nl.sotallytober.sotallytober.R;
import nl.sotallytober.sotallytober.database.models.UserModel;
import nl.sotallytober.sotallytober.database.providers.UserProvider;
import nl.sotallytober.sotallytober.helpers.LogHelper;
import nl.sotallytober.sotallytober.http.Connection;
import nl.sotallytober.sotallytober.http.Response;

/**
 * Tries to register a UserModel
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class RegisterUserAsyncTask extends AsyncTask<Void, Void, UserModel> {

    private final OnUserRegisterAsyncTaskListener mCallback;

    private final HashMap<String, String> mUserDetails = new HashMap<>();

    private final String mRequestURL = "/auth/register";

    private Response mResponse;

    public RegisterUserAsyncTask(OnUserRegisterAsyncTaskListener mCallback, String email,
                                 String password, String passwordConfirmation, String firstName,
                                 String insertion, String lastName, String dateOfBirth) {
        this.mCallback = mCallback;
        this.mUserDetails.put("email", email);
        this.mUserDetails.put("password", password);
        this.mUserDetails.put("password_confirmation", passwordConfirmation);
        this.mUserDetails.put("first_name", firstName);
        this.mUserDetails.put("insertion", insertion);
        this.mUserDetails.put("last_name", lastName);
        this.mUserDetails.put("date_of_birth", dateOfBirth);
    }

    public interface OnUserRegisterAsyncTaskListener {
        void onUserRegisterAsyncTaskSuccess(UserModel user);
        void onUserRegisterAsyncTaskFailed(Response mResponse);
    }

    @Override
    protected UserModel doInBackground(Void... params) {
        mResponse = new Connection(mRequestURL, Connection.REQUEST_METHOD_POST, mUserDetails).getResponse();

        if(!mResponse.getStatus().equals(Response.STATUS_SUCCESS)) {
            return null;
        }

        UserModel userModel;
        try {
            JSONObject user = new JSONObject(mResponse.getData());
            userModel = new UserModel(user, mResponse.getSessionCookie(), mResponse.getRememberMeCookie());
        } catch (JSONException e) {
            LogHelper.printStackTrace(e);
            mResponse = new Response(Response.STATUS_ERROR_MESSAGE, R.string.error_http_json_exception);
            return null;
        }

        UserProvider.get().insertOrUpdate(userModel);
        return userModel;
    }

    @Override
    protected void onPostExecute(UserModel user) {
        super.onPostExecute(user);

        if(user != null) {
            mCallback.onUserRegisterAsyncTaskSuccess(user);
        } else {
            mCallback.onUserRegisterAsyncTaskFailed(mResponse);
        }
    }

}
