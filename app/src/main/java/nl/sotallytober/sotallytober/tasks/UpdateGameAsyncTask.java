package nl.sotallytober.sotallytober.tasks;

import android.os.AsyncTask;

import nl.sotallytober.sotallytober.database.models.GameModel;
import nl.sotallytober.sotallytober.database.providers.GameProvider;

/**
 * Updates a GameModel
 *
 * @author Tijme Gommers
 * @version 1.0.0
 * @date 13-05-15
 */
public class UpdateGameAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final OnUpdateGameAsyncTaskListener mCallback;

    private final GameModel mGame;

    public UpdateGameAsyncTask(OnUpdateGameAsyncTaskListener mCallback, GameModel mGame) {
        this.mCallback = mCallback;
        this.mGame = mGame;
    }

    public interface OnUpdateGameAsyncTaskListener {
        void onUpdateGameAsyncTaskCompleted();
        void onUpdateGameAsyncTaskFailed(GameModel mOldGame);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            Thread.sleep(2000);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);

        if(success) {
            mCallback.onUpdateGameAsyncTaskCompleted();
        } else {
            mCallback.onUpdateGameAsyncTaskFailed(mGame);
        }
    }

}
